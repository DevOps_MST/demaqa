<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Mission_Rejected</fullName>
        <description>Mission Rejected</description>
        <protected>false</protected>
        <recipients>
            <field>Mission_Coordinator__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DEMA/Mission_Rejected</template>
    </alerts>
    <alerts>
        <fullName>X10_Day_Prior_to_Close</fullName>
        <description>10 Day Prior to Close</description>
        <protected>false</protected>
        <recipients>
            <field>Mission_Coordinator__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Aiding_Agency_Coordinator_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Finance_User_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DEMA/X10_Days_Prior_to_Close</template>
    </alerts>
    <alerts>
        <fullName>X1_Day_Prior_to_Close</fullName>
        <description>1 Day Prior to Close</description>
        <protected>false</protected>
        <recipients>
            <field>Mission_Coordinator__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Aiding_Agency_Coordinator_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Finance_User_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DEMA/X1_Days_Prior_to_Close</template>
    </alerts>
    <alerts>
        <fullName>X30_Days_After_Start_Date</fullName>
        <description>30 Days After Start Date</description>
        <protected>false</protected>
        <recipients>
            <field>Mission_Coordinator__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Aiding_Agency_Coordinator_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Finance_User_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DEMA/X30_Days_After_Start_Date</template>
    </alerts>
    <alerts>
        <fullName>X61_Days_After_Start_Date</fullName>
        <description>61 Days After Start Date</description>
        <protected>false</protected>
        <recipients>
            <recipient>test-lty1pxefnzob@mstdemo.net</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DEMA/X61_Days_After_Start_Date</template>
    </alerts>
    <fieldUpdates>
        <fullName>Approve_Mission</fullName>
        <field>Mission_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Approve Mission</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Mission_Rejection</fullName>
        <field>Mission_Status__c</field>
        <literalValue>Not Approved</literalValue>
        <name>Mission Rejection</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
</Workflow>
